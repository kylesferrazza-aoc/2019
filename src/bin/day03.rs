use advent::input::read_day_csvs;
use std::collections::HashMap;
use std::ops::Add;

fn main() {
    let (one, two) = inp();
    let res1 = shortest_manhattan(one, two);
    println!("part1: {}", res1);
    let (one, two) = inp();
    let res2 = shortest_wire(one, two);
    println!("part2: {}", res2);
}

fn inp() -> (Vec<Move>, Vec<Move>) {
    split_moves(read_day_csvs("3"))
}

fn split_moves(all_moves: Vec<Vec<String>>) -> (Vec<Move>, Vec<Move>) {
    let mut moves = parse_strs(all_moves);
    let wire_one_moves: Vec<Move> = moves.remove(0);
    let wire_two_moves: Vec<Move> = moves.remove(0);
    (wire_one_moves, wire_two_moves)
}

fn parse_strs(strs: Vec<Vec<String>>) -> Vec<Vec<Move>> {
    strs.iter()
        .map(|wire| wire.iter().map(|m| parse_move(m)).collect())
        .collect()
}

#[derive(Debug, PartialEq, Eq)]
enum Direction {
    L,
    R,
    D,
    U,
}

#[derive(Debug, PartialEq, Eq)]
struct Move {
    direction: Direction,
    steps: i64,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn origin() -> Point {
        Point { x: 0, y: 0 }
    }
}

impl Add<&Direction> for Point {
    type Output = Self;

    fn add(self, dir: &Direction) -> Self {
        let mut x = self.x;
        let mut y = self.y;
        match dir {
            Direction::U => y += 1,
            Direction::D => y -= 1,
            Direction::L => x -= 1,
            Direction::R => x += 1,
        }
        Point { x, y }
    }
}

struct PointInfo {
    wires: HashMap<String, i64>,
}

struct Grid {
    grid: HashMap<Point, PointInfo>,
}

impl Grid {
    fn new() -> Grid {
        Grid {
            grid: HashMap::new(),
        }
    }

    fn touch_point(self: &mut Self, point: Point, name: &str, len: i64) {
        self.grid
            .entry(point)
            .or_insert(PointInfo::zero())
            .visit(name.to_string(), len);
    }

    fn perform_moves(self: &mut Self, moves: &Vec<Move>, name: &str) {
        let mut pointer = Point { x: 0, y: 0 };
        let mut len: i64 = 0;
        for m in moves {
            for _ in 0..m.steps {
                len += 1;
                pointer = pointer + &m.direction;
                self.touch_point(pointer, name, len);
            }
        }
    }

    fn intersections(self: &Self) -> Vec<(&Point, &PointInfo)> {
        self.grid
            .iter()
            .filter_map(|entry| {
                let (_, info): (&Point, &PointInfo) = entry;
                if info.wires.len() > 1 {
                    Some(entry)
                } else {
                    None
                }
            })
            .collect()
    }
}

impl PointInfo {
    fn zero() -> PointInfo {
        PointInfo {
            wires: HashMap::new(),
        }
    }

    fn visit(self: &mut PointInfo, name: String, len: i64) {
        let found = self.wires.keys().find(|k| *k == &name);
        match found {
            Some(_) => {
                // double visit
            }
            None => {
                self.wires.insert(name, len);
            }
        }
    }
}

fn manhattan(p1: Point, p2: Point) -> i64 {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

fn parse_move(s: &str) -> Move {
    let chars: Vec<char> = s.chars().collect();
    let letter: char = chars[0];
    let num_s: String = Vec::from(&chars[1..]).iter().collect();
    let num: i64 = num_s.parse().unwrap();
    let dir = match letter {
        'L' => Direction::L,
        'R' => Direction::R,
        'D' => Direction::D,
        'U' => Direction::U,
        x => panic!("Invalid direction: {}", x),
    };
    Move {
        direction: dir,
        steps: num,
    }
}

fn shortest_manhattan(one: Vec<Move>, two: Vec<Move>) -> i64 {
    let mut grid: Grid = Grid::new();

    grid.perform_moves(&one, "A");
    grid.perform_moves(&two, "B");

    let intersections = grid.intersections();

    let mut distances: Vec<i64> = intersections
        .iter()
        .map(|(p, _)| manhattan(Point::origin(), **p))
        .collect();
    distances.sort();

    *distances.get(0).unwrap()
}

fn shortest_wire(one: Vec<Move>, two: Vec<Move>) -> i64 {
    let mut grid: Grid = Grid::new();

    grid.perform_moves(&one, "A");
    grid.perform_moves(&two, "B");

    let intersections = grid.intersections();

    let mut combined_steps: Vec<i64> = intersections
        .iter()
        .map(|(_, info)| info.wires.values().sum())
        .collect();
    combined_steps.sort();

    *combined_steps.get(0).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn ex_1() -> Vec<Vec<&'static str>> {
        vec![
            vec!["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
            vec!["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
        ]
    }

    fn ex_2() -> Vec<Vec<&'static str>> {
        vec![
            vec![
                "R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51",
            ],
            vec![
                "U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7",
            ],
        ]
    }

    fn assert_part1(wires: Vec<Vec<&str>>, dist: i64) {
        let strs: Vec<Vec<String>> = wires
            .iter()
            .map(|wire| wire.iter().map(|s| s.to_string()).collect())
            .collect();
        let (one, two) = split_moves(strs);
        assert_eq!(shortest_manhattan(one, two), dist);
    }

    fn assert_part2(wires: Vec<Vec<&str>>, dist: i64) {
        let strs: Vec<Vec<String>> = wires
            .iter()
            .map(|wire| wire.iter().map(|s| s.to_string()).collect())
            .collect();
        let (one, two) = split_moves(strs);
        assert_eq!(shortest_wire(one, two), dist);
    }

    #[test]
    fn test_eq() {
        assert_eq!(Point { x: 2, y: 6 }, Point { x: 2, y: 6 });
        assert_ne!(Point { x: 3, y: 6 }, Point { x: 2, y: 6 });
    }

    #[test]
    fn test_part1() {
        assert_part1(ex_1(), 159);
        assert_part1(ex_2(), 135);
    }
    #[test]
    fn test_part2() {
        assert_part2(ex_1(), 610);
        assert_part2(ex_2(), 410);
    }

    #[test]
    fn test_answer() {
        let (one, two) = inp();
        let res = shortest_manhattan(one, two);
        assert_eq!(res, 709);
    }
}
