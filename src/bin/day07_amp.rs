use advent::input::read_day_csv;
use advent::intcode::Computer;

fn main() {
    let inp = read_day_csv("7");
    let mut cpu = Computer::new();
    cpu.load_in(inp);
    cpu.run();
}
