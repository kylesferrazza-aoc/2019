use advent::input::read_day;

fn main() {
    println!("part 1: {}", part1());
    println!("part 2: {}", part2());
}

fn fuel(mass: i64) -> i64 {
    mass / 3 - 2
}

fn part1() -> i64 {
    go(fuel)
}

fn part2() -> i64 {
    go(running_fuel)
}

fn go(f: fn(i64) -> i64) -> i64 {
    read_day("1").iter().map(|x| *x).map(f).sum()
}

fn running_fuel(mass: i64) -> i64 {
    let orig = fuel(mass);
    if orig <= 0 {
        return 0;
    } else {
        return orig + running_fuel(orig);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fuel_tests() {
        assert_eq!(fuel(12), 2);
        assert_eq!(fuel(14), 2);
        assert_eq!(fuel(1969), 654);
        assert_eq!(fuel(100756), 33583);
        assert_eq!(fuel(94164), 31386)
    }

    #[test]
    fn running_fuel_tests() {
        assert_eq!(running_fuel(14), 2);
        assert_eq!(running_fuel(1969), 966);
        assert_eq!(running_fuel(100756), 50346);
    }

    #[test]
    fn test_answers() {
        assert_eq!(part1(), 3414791);
        assert_eq!(part2(), 5119312);
    }
}
