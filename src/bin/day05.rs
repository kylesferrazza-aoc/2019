use advent::input::read_day_csv;
use advent::intcode::Computer;

fn main() {
    part1();
}

fn part1() {
    let mut cpu = Computer::new();
    let inp: Vec<isize> = read_day_csv("5");
    cpu.load_in(inp);
    cpu.run();
}
