use advent::input::read_day;
use std::collections::HashMap;

fn main() {
    println!("part1: {}", part1(read_day("6")));
}

/// map of planet to the planet it orbits
fn read_in<'a>(inp: &'a Vec<String>) -> HashMap<&str, &str> {
    // input data as tuples
    let mut map = HashMap::new();
    inp.iter().for_each(|s| {
        let mut split: Vec<&str> = s.split(")").collect();
        let res: (&str, &str) = (split.remove(0), split.remove(0));
        map.insert(res.1, res.0);
    });
    map
}

fn part1(inp: Vec<String>) -> usize {
    let map = read_in(&inp);

    let mut res: HashMap<&str, usize> = HashMap::new();
    res.insert("COM", 0);

    // for each planet,
    // calculate distance to COM
    // and then sum these distances

    for pair in map.iter() {
        let (planet, orbiting): (&&str, &&str) = pair;
        if res.contains_key(planet) {
            continue;
        }
        if res.contains_key(orbiting) {
            let val = res.get(orbiting).unwrap();
            res.insert(planet, val + 1);
            continue;
        }
        let mut distance = 1;
        let mut current: &str = orbiting;
        loop {
            if current.eq("COM") {
                break;
            }
            distance += 1;
            current = map.get(current).unwrap();
        }
        res.insert(planet, distance);
    }

    res.values().sum()
}

fn part2(inp: Vec<String>) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        assert_eq!(part1(read_day("6_small")), 42);
    }

    #[test]
    fn test_example_2() {
        // assert_eq!(part2(read_day("6_2_small")), 42);
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(read_day("6")), 227612);
    }
}
