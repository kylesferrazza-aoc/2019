use std::io::{BufWriter, Write};
use std::str::from_utf8;

use std::process::{Command, Stdio};

fn main() {
    println!("part1: {}", part1());
}

const EXE: &str = "target/debug/day07_amp";

fn run_one(inp: usize, phase: usize) -> usize {
    let mut child = Command::new(EXE)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();
    let mut stdin = BufWriter::new(child.stdin.take().unwrap());
    stdin
        .write_fmt(format_args!("{}\n{}\n", phase, inp))
        .unwrap();
    stdin.flush().unwrap();

    let out = child.wait_with_output().unwrap();

    let s = from_utf8(&out.stdout).unwrap().trim();
    s.parse().unwrap()
}

fn run_phaser(a: usize, b: usize, c: usize, d: usize, e: usize) -> usize {
    let a_out = run_one(0, a);
    let b_out = run_one(a_out, b);
    let c_out = run_one(b_out, c);
    let d_out = run_one(c_out, d);
    let e_out = run_one(d_out, e);
    e_out
}

fn part1() -> usize {
    let mut max = 0;
    let mut best_conf = (0, 0, 0, 0, 0);
    for a in 0..5 {
        for b in 0..5 {
            if b == a {
                continue;
            }
            for c in 0..5 {
                if c == b || c == a {
                    continue;
                }
                for d in 0..5 {
                    if d == c || d == b || d == a {
                        continue;
                    }
                    for e in 0..5 {
                        if e == d || e == c || e == b || e == a {
                            continue;
                        }
                        let res = run_phaser(a, b, c, d, e);
                        if res > max {
                            max = res;
                            best_conf = (a, b, c, d, e);
                        }
                    }
                }
            }
        }
    }
    eprintln!("conf: {:?}", best_conf);
    max
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 45730);
    }
}
