//! Helpers for reading input from files in various formats.

use std::fs::File;
use std::io::{BufRead, BufReader};

const IN_DIR: &str = "in/";

fn read_lines(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    BufReader::new(file).lines().map(|l| l.unwrap()).collect()
}

/// Read the input linewise for a given day as a `Vec<T>`.
pub fn read_day<T: std::str::FromStr>(day: &str) -> Vec<T> {
    let filename = IN_DIR.to_string() + day;
    let lines: Vec<String> = read_lines(&filename);
    lines.iter().map(|f| f.parse().ok().unwrap()).collect()
}

/// Read one comma-separated line of input for a given day as a `Vec<T>`.
pub fn read_day_csv<T: std::str::FromStr>(day: &str) -> Vec<T> {
    let mut csvs = read_day_csvs(day);
    csvs.remove(0)
}

/// Read multiple comma-separated lines of input for a given day as a `Vec<Vec<T>>`.
pub fn read_day_csvs<T: std::str::FromStr>(day: &str) -> Vec<Vec<T>> {
    let lines: Vec<String> = read_day(day);
    lines
        .iter()
        .map(|line| {
            let ints: Vec<T> = line
                .split(',')
                .map(|s| T::from_str(s).ok().unwrap())
                .collect();
            ints
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_day_str() {
        let lines: Vec<String> = read_day("test");
        assert_eq!(lines.len(), 2);
        assert_eq!(lines[0], "hello");
        assert_eq!(lines[1], "goodbye");
    }

    #[test]
    fn test_read_day_int() {
        let lines: Vec<u64> = read_day("test_num");
        assert_eq!(lines.len(), 2);
        assert_eq!(lines[0], 32);
        assert_eq!(lines[1], 854);
    }
}
